package com.epam.rd.autotasks;

import org.junit.Assert;
import org.junit.Test;


public class SortingTest {

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void testNullCase(){
        sorting.sort(null);
    }

    @Test
    public void testEmptyCase(){
        int[] res = {};
        sorting.sort(res);
        int[] expect = {};
        Assert.assertArrayEquals(expect,res);
    }

    @Test
    public void testSingleElementArrayCase() {
        int[] a = {1};
        sorting.sort(a);
        int[] aExpect = {1};
        Assert.assertArrayEquals(aExpect,a);
        int[] b = {2};
        sorting.sort(b);
        int[] bExpect = {2};
        Assert.assertArrayEquals(bExpect,b);
        int[] c = {3};
        sorting.sort(c);
        int[] cExpect = {3};
        Assert.assertArrayEquals(cExpect,c);
    }

    @Test
    public void testSortedArraysCase() {
        int[] a = {1,2,3};
        sorting.sort(a);
        int[] aExpect = {1,2,3};
        Assert.assertArrayEquals(aExpect,a);
        int[] b = {1,2,2,4};
        sorting.sort(b);
        int[] bExpect = {1,2,2,4};
        Assert.assertArrayEquals(bExpect,b);
        int[] c = {3,3,3,3,3,3,3,3};
        sorting.sort(c);
        int[] cExpect = {3,3,3,3,3,3,3,3};
        Assert.assertArrayEquals(cExpect,c);
    }

    @Test
    public void testOtherCases() {
        int[] a = {3,2,1};
        sorting.sort(a);
        int[] aExpect = {1,2,3};
        Assert.assertArrayEquals(aExpect,a);
        int[] b = {4,1,2,2,3,4};
        sorting.sort(b);
        int[] bExpect = {1,2,2,3,4,4};
        Assert.assertArrayEquals(bExpect,b);
        int[] c = {3,3,3,3,3,3,3,3,1};
        sorting.sort(c);
        int[] cExpect = {1,3,3,3,3,3,3,3,3};
        Assert.assertArrayEquals(cExpect,c);
    }

}